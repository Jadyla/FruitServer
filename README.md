This project is the low-level webserver that runs on the Controllar home automation installation.
It is the bridge between the serial port and the docker that communicates with the external server.

It is meant to use with one of the Controllar's base images.

You also need a `.db3` file provided by Controllar which describes your home automation and a `.ovpn` to connect to the system.

This software should already be installed in one of the Controllar's base images.

To configure, copy the `.db3` and `.ovpn` files to `/controllar` folder, than, by ssh:

```
cd /controllar
./maleta.sh # select option 1, 3, 1
reboot
```

## Configuring the IR/RF commands

The `maleta.sh` script already configures the following multimedia options on config.ini:

```
mqtt_broker_port= 1883"
irconfig_file=<your-instalation-name>.yaml
broadlink_cmd_folder=/controllar/broadlink-mqtt/commands
encodeir=/controllar/MakeHex/encodeir
```

* `mqtt_broaker` and `mqtt_broaker_port` are the location for the mqtt broker
  such as mosquitto.
* `irconfig_file` is the yaml that configures the ir/rf signals (see below).
* `broadlink_cmd_folder` points to where the commands are and will be saved,
  usually inside broadlink-mqtt instalation folder. The signals are converted to
  the broadlink format.
* `encodeir` is the binary that convert a protocol format to raw.

To configure Luminos to emit IR/RF commands, configure an yaml file on `/controllar` folder.

Example:

```
    1:
      emitter: broadlink
      command: tv/samsung/volumeup
    7:
      emitter: broadlink
      command: tv/samsung/volumedown
    28:
      signal: rf
      emitter: broadlink
      command: rfkit/bt1
    29:
      emitter: thmedia
      command: tv/samsung/mute
      channel: 2
    3:
      emitter: broadlink
      command:
        protocol: NECx2
        device: 7
        subdevice: 7
        function: 15
```

Signals are saved under `broadlink_cmd_folder/`

default values are:
    signal: ir
    channel: 1
    freq: 38000

The frequency is defined on broadlink format header and current ignored by thmedia.

## Communication with the intel firmware

### Read painels and intels connected
Command: $CFGBID?>Bc*.*<br>
Response: <b>InGGG.ID</b> or <b>PnGGG.ID</b>
- GGG: group
- ID: id

Example:
```
In050.01Pn001.01Pn002.01Pn001.02In050.02
```

### Read all channels information
Command: $CFGACH?>InGGG.ID<br>
Response: <b>InGGG.ID,CANAL,TIPO,LUMINO</b>
- GGG: group
- ID: id

Example:
```
In050.01,01,01,00001In050.01,02,01,00001In050.01,03,01,00003In050.01,04,01,00004In050.01,05,01,00005In050.01,06,01,00006In050.01,07,01,00007In050.01,08,01,00008In050.01,09,01,00009In050.01,10,01,00010In050.01,11,01,00011In050.01,12,01,00012In050.01,13,01,00013In050.01,14,01,00014In050.01,15,01,00015In050.01,16,01,00016In050.01,17,01,00017In050.01,18,01,00018In050.01,19,01,00019In050.01,20,01,00020In050.01,21,01,00021In050.01,22,01,00022In050.01,23,01,00023In050.01,24,01,00024In050.01,25,01,00025In050.01,26,01,00026In050.01,27,01,00027In050.01,28,01,00028In050.01,29,01,00029In050.01,30,01,00030
```

### Read all buttons information
Command: $CFGABT?>InGGG.ID<br>
Response: <b>InGGG.ID,BOTÃO,LUMINO,TIPO</b>
- GGG: group
- ID: id
- TIPO: button type (0- undefined / 1 -scene / 2- Digital Sensor NA / 3- Digital Sensor NF / 4- protected scene)

Example:
```
In050.01,01,00000,00In050.01,02,00000,00In050.01,03,00000,00In050.01,04,00000,00In050.01,05,00000,00In050.01,06,00000,00In050.01,07,00000,00In050.01,08,00000,00In050.01,09,00000,00In050.01,10,00000,00In050.01,11,00000,00In050.01,12,00000,00In050.01,13,00000,00In050.01,14,00000,00In050.01,15,00000,00In050.01,16,00000,00In050.01,17,00000,00In050.01,18,00000,00In050.01,19,00000,00In050.01,20,00000,00In050.01,21,00000,00In050.01,22,00000,00In050.01,23,00000,00In050.01,24,00000,00In050.01,25,00000,00In050.01,26,00000,00In050.01,27,00000,00In050.01,28,00000,00In050.01,29,00000,00In050.01,30,00000,00In050.01,31,00000,00In050.01,32,00000,00In050.01,33,00000,00In050.01,34,00000,00In050.01,35,00000,00In050.01,36,00000,00

```

### Listen KEYPAD button press
Command: $SNF<br>
Comment: it can contain no lumino or scene information (empty), only lumino info (normal button) or only scene (button scene type)<br>
Response: <b>PnGGG.ID-BTN</b> e (<b>LLN</b> or <b>SST</b>)
- GGG: group
- ID: id
- BTN: button number
- LN: lumino number
- ST: scene type (1- scene on / 2- scene off)

Example:
```
Pn002.01-13
Pn002.01-13 L1
Pn002.01-13 S1
```

### See if the scene on KEYPAD is protected
Command: $CFGPTS?>PnGGG.ID,BOTÃO<br>
- GGG: group
- ID: id
Response: <b>PnGGG.ID,BOTÃO,FIXED</b>

Example:
```
Pn002.01,27,0
Pn002.01,27,1
```

### List virtual luminos
Command: $CFGVRL?>InGGG.ID<br>
- GGG: group
- ID: id
Response: <b>InGGG.ID,LUMINO,TIPO</b>

Example:
```
In050.01,00600,01
In050.01,00603,01
```

### Associate lumino to channel
Command: $CFGCLU=>InGGG.ID,LUMINO,CANAL<br>
Response: -

### Associate button to channel (also to KEYPAD)
Command: $CFGOLU=>PnGGG.ID,BOTÃO,1(para feedback),1(acionamento),LUMINO,0,0<br>
Response: -

### Define button as scene
Command: $CFGSTP=>PnGGG.ID,BOTÃO,1(1 para cena on),SETOR<br>
Command: $CFGSTP=>PnGGG.ID,BOTÃO,2(2 para cena off),SETOR<br>
Response: -

### Set channel type
Command: $CFGCTY=>InGGG.ID,LUMINO,TIPO<br>
Response: -

### Set button as monitoring type
Command: $CFGDSC=>PnGGG.ID,LUMINO,BOTÃO,TIPO<br>
- TIPO 1: NA
- TIPO 2: NF
Response: -

### Format intel saving ID information
Command: $CFGFSI+>InGGG.ID<br>
Response: -

### Protect scene on KEYPAD
Command: $CFGPTS=>PnGGG.ID,BUTTON,FIXED<br>
- GGG: group
- ID: id
- FIXED: boolean isProtected?
Response: -

### Create virtual lumino
Command: $CFGVRL=>InGGG.ID,LUMINOID,TIPO
- GGG: group
- ID: id
- LUMINOID: virtual lumino's id, normally between 600 and 799 (at the moment limited to 40 virtual luminos)
Response: -

### Edit KEYPAD/Intel id
Command: $CFGBID=>InGGG.ID,NOVOID
- GGG: group
- ID: id
- NOVOID: new id that now will be the board ID
Response: -

### Edit KEYPAD/Intel group
Command: $CFGBGR=>InGGG.ID,NOVOGRUPO
- GGG: group
- ID: id
- NOVOGRUPO: new group that now will be the board GROUP
Response: -